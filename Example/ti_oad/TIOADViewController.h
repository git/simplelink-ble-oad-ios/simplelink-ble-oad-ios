/*
 Copyright 2018 Texas Instruments
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

@import UIKit;

#import <Corebluetooth/CoreBluetooth.h>
#import <TIOAD.h>

@interface TIOADViewController : UIViewController <CBCentralManagerDelegate,CBPeripheralDelegate,TIOADClientProgressDelegate,UITableViewDelegate,UITableViewDataSource>

@property CBCentralManager *man;
@property CBPeripheral *perip;

@property NSMutableArray *deviceList;

@property (weak, nonatomic) IBOutlet UIProgressView *TIOADProgress;
@property (weak, nonatomic) IBOutlet UILabel *TIOADCurrentStatus;
@property (weak, nonatomic) IBOutlet UILabel *TIOADOADImage;
@property (weak, nonatomic) IBOutlet UILabel *TIOADCurrentBlock;
@property (weak, nonatomic) IBOutlet UILabel *TIOADTotalBlock;
@property (weak, nonatomic) IBOutlet UILabel *TIOADMTUSize;
@property (weak, nonatomic) IBOutlet UILabel *TIOADBlockSize;
@property (weak, nonatomic) IBOutlet UITextView *TIOADImageInfo;
@property (weak, nonatomic) IBOutlet UITableView *TIOADDevicesFoundTableView;
@property (weak, nonatomic) IBOutlet UILabel *TIOADChipID;
@property (weak, nonatomic) IBOutlet UILabel *TIOADTotalBytes;
@property (weak, nonatomic) IBOutlet UILabel *TIOADCurrentByte;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *TIOADActivityIndicator;


@property (weak, nonatomic) IBOutlet UIButton *TIOADStartScanButton;
- (IBAction)TIOADStartScanButtonTouched:(id)sender;







@end

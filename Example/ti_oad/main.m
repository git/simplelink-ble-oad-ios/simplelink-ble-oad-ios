//
//  main.m
//  ti_oad
//
//  Created by Ole Andreas Torvmark on 10/24/2017.
//  Copyright (c) 2017 Ole Andreas Torvmark. All rights reserved.
//

@import UIKit;
#import "TIOADAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TIOADAppDelegate class]));
    }
}

/*
 Copyright 2018 Texas Instruments
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import "TIOADViewController.h"
#import "TIOADDeviceTableViewCell.h"

@interface TIOADViewController ()

@end

@implementation TIOADViewController {
    TIOADToadImageReader *oadImage;
    TIOADClient *client;
    BOOL deviceSelected;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //AGAMA
    oadImage = [[TIOADToadImageReader alloc] initWithImageData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource: @"ble5_project_zero_cc26x2r1lp_app_FlashROM_Release_oad.bin" ofType:@"bin"]] fileName:[[NSBundle mainBundle] pathForResource: @"ble5_project_zero_cc26x2r1lp_app_FlashROM_Release_oad.bin" ofType:@"bin"]];
    self.TIOADOADImage.text = [[[NSBundle mainBundle] pathForResource: @"ble5_project_zero_cc26x2r1lp_app_FlashROM_Release_oad.bin" ofType:@"bin"] lastPathComponent];
    self.TIOADImageInfo.text = [oadImage description];
    self.TIOADDevicesFoundTableView.delegate = self;
    self.TIOADDevicesFoundTableView.dataSource = self;
    self.deviceList = [[NSMutableArray alloc] init];
    deviceSelected = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state == CBManagerStatePoweredOn) {
        [central scanForPeripheralsWithServices:nil options:nil];
    }
}

-(void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
    if ([RSSI integerValue] > - 80) {
        BOOL found = false;
        for (int ii = 0; ii < self.deviceList.count; ii++) {
            TIOADDeviceTableViewCell *cell = self.deviceList[ii];
            if ([cell.p.identifier isEqual:peripheral.identifier]) {
                found = true;
            }
        }
        if (!found) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TIOADDeviceTableViewCell" owner:self options:nil];
            TIOADDeviceTableViewCell *cell = (TIOADDeviceTableViewCell *)[nib objectAtIndex:0];
            cell.p = peripheral;
            cell.deviceNameLabel.text = peripheral.name;
            cell.deviceUUIDLabel.text = peripheral.identifier.UUIDString;
            [self.deviceList addObject:cell];
            [self.TIOADDevicesFoundTableView reloadData];
        }
    }
}

-(void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    _perip = peripheral;
    _perip.delegate = self;
    [peripheral discoverServices:[NSArray arrayWithObjects:[CBUUID UUIDWithString:TI_OAD_SERVICE], nil]];

}

-(void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    for (CBService *s in peripheral.services) {
        [peripheral discoverCharacteristics:nil forService:s];
    }
}

-(void) peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    if ([service.UUID.UUIDString isEqualToString:TI_OAD_SERVICE]) {
        NSLog(@"Start OAD, we are ready");
        client = [[TIOADClient alloc] initWithPeripheral:peripheral andImageData:oadImage andDelegate:self];
        self.TIOADMTUSize.text = [NSString stringWithFormat:@"%ld", (long)[peripheral maximumWriteValueLengthForType:CBCharacteristicWriteWithoutResponse]];
        self.TIOADBlockSize.text = [NSString stringWithFormat:@"%ld", (long)[peripheral maximumWriteValueLengthForType:CBCharacteristicWriteWithoutResponse] - 4];
    }
}


-(void) client:(TIOADClient *)client oadProgressUpdated:(TIOADClientProgressValues_t)progress {
    NSLog(@"Progress update: ");
    NSLog(@"Block %d of %d",progress.currentBlock, progress.totalBlocks);
    NSLog(@"Byte %d og %d",progress.currentByte, progress.totalBytes);
    NSLog(@"Progress: %0.1f",(float)progress.currentByte/(float)progress.totalBytes);
    [self.TIOADActivityIndicator stopAnimating];
    self.TIOADProgress.hidden = NO;
    
    self.TIOADTotalBlock.text = [NSString stringWithFormat:@"%d",progress.totalBlocks];
    self.TIOADCurrentBlock.text = [NSString stringWithFormat:@"%d",progress.currentBlock];
    self.TIOADTotalBytes.text = [NSString stringWithFormat:@"%d",progress.totalBytes];
    self.TIOADCurrentByte.text = [NSString stringWithFormat:@"%d",progress.currentByte];
    self.TIOADProgress.progress = (float)progress.currentByte/(float)progress.totalBytes;
    
    
}

-(void) client:(TIOADClient *)client oadProcessStateChanged:(TIOADClientState_t)state error:(NSError *)error {
    NSLog(@"State changed : %d",(int)state);
    NSLog(@"Error: %@",error);

    if ((state == tiOADClientGetDeviceTypeResponseRecieved) && error == nil) {
        self.TIOADStartScanButton.enabled = YES;
        self.TIOADChipID.text = [client getChipId];
    }
    
    self.TIOADCurrentStatus.text = [TIOADClient getStateStringFromState:state];
    
    if (error) {
        UIAlertController *aC = [UIAlertController alertControllerWithTitle:@"Error !"
                                                                    message:[NSMutableString stringWithFormat:@"EOAD Process failed with error: \"%@\"",error.localizedDescription]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
        [aC addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
        }]];
        [self showViewController:aC sender:self];
    }
}





-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.deviceList.count;
}
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.deviceList[indexPath.row];
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TIOADDeviceTableViewCell *cell = self.deviceList[indexPath.row];
    [self.man connectPeripheral:cell.p options:nil];
    deviceSelected = YES;
    self.TIOADStartScanButton.enabled = NO;
    [self.TIOADStartScanButton setTitle:@"Start Programming" forState:UIControlStateNormal];

}



- (IBAction)TIOADStartScanButtonTouched:(id)sender {
    if (!deviceSelected) {
        self.deviceList = [[NSMutableArray alloc] init];
        deviceSelected = NO;
        [self.TIOADDevicesFoundTableView reloadData];
        self.man = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    }
    else {
        [client startOAD];
        [self.TIOADActivityIndicator startAnimating];
        self.TIOADProgress.hidden = YES;
    }
    
}
@end

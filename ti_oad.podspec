#
# Be sure to run `pod lib lint ti_oad.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ti_oad'
  s.version          = '1.1.0'
  s.summary          = 'Over the Air Download library for CC26xx/CC13xx'

  s.description      = <<-DESC
Texas Instruments OAD (Over the Air Download) library.
This pod handles firmware updates on the CC26xx/CC13xx devices
ti_oad makes it easy for customers to integrate firmware update
into their existing iOS projects. For platform support notes, please see README.md
                    DESC

  s.homepage         = 'https://git.ti.com/simplelink-ble-oad-ios'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'Apache 2.0', :file => 'LICENSE' }
  s.author           = { 'Ole Andreas Torvmark' => 'o.a.torvmark@ti.com' }
  s.source           = { :git => 'git://git.ti.com/simplelink-ble-oad-ios/simplelink-ble-oad-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.macos.deployment_target = '10.13'

  s.source_files = 'ti_oad/Classes/**/*'
  
  # s.resource_bundles = {
  #   'ti_oad' => ['ti_oad/Assets/*.png']
  # }

  s.public_header_files = 'ti_oad/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
